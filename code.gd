extends MeshInstance3D

## Makes a 'rope'
## Might not be the best or fastest way, but it's what I came up with.
## HTH

@onready var camera:Camera3D = $"../Camera3D"
@onready var box = $"../box"

var rope:Rope
var mousepos:Vector3

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _input(event):
	if event is InputEventKey:
		get_tree().quit()


	if event is InputEventMouseMotion:
		var mouse_position = event.relative
		mouse_position *= Vector2(1,-1)
		mousepos += Vector3(mouse_position.x, mouse_position.y, 0) / 1000.
		box.position = mousepos
		make_rope(mousepos, Vector3.ZERO)


func make_rope(from, to):
	rope = Rope.new(from, to)
	mesh = rope.mesh
	position = from

	# The reason the rope must lay along Z going into the screen is
	# because the look_at function requires this.
	# We also make an 'up' vector to feed-into look_at. This is
	# a bit weird but it makes sure there's something very close to
	# up relative to our Transform.
	var upish:Vector3 = (transform.basis.y + transform.basis.z).normalized()
	look_at(to, upish)


class Rope:
	var mesh : Mesh

	func _init(from:Vector3, to:Vector3) -> void:
		var length = (from-to).length()
		var radius := 0.05
		var rope := CylinderMesh.new()
		# At this point, the cylinder is standing up along the Y axis
		# Its origin is in the middle of the cylinder too.

		rope.radial_segments = 8
		rope.bottom_radius = radius
		rope.top_radius = radius
		rope.cap_bottom = false
		rope.height = length
		var rope_array = rope.get_mesh_arrays()
		for i in range(0,rope_array[0].size()):
			# Move the cylinder up so its end is the origin
			rope_array[ArrayMesh.ARRAY_VERTEX][i] -= Vector3(0,length/2.0,0)
			# Rotate it so it lays along the Z pointing into screen (very NB)
			rope_array[ArrayMesh.ARRAY_VERTEX][i] = rope_array[ArrayMesh.ARRAY_VERTEX][i].rotated(Vector3.RIGHT, PI/2.0)
			# Also rotate the normals. I think. Test it yourself.
			rope_array[ArrayMesh.ARRAY_NORMAL][i] = rope_array[ArrayMesh.ARRAY_NORMAL][i].rotated(Vector3.RIGHT, PI/2.0)

		# Put it all back into an ArrayMesh
		var arr_mesh = ArrayMesh.new()
		arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, rope_array)
		mesh = arr_mesh
